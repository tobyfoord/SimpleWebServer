﻿Module Module1

    Sub Main(ByVal args As String())
        If args.Length <> 2 Then
            LogEvent("Error: Please provide a path and port", ConsoleColor.Red)
            Exit Sub
        End If


        Dim RootPath As String = args(0)
        Dim Port As Integer

        RootPath = IO.Path.Combine(Environment.CurrentDirectory, RootPath)
        If Not IO.Directory.Exists(RootPath) Then
            LogEvent("Error: Path not found.", ConsoleColor.Red)
            Exit Sub
        End If
        
        If Not Integer.TryParse(args(1), Port) Then
            LogEvent("Error: Port not valid.", ConsoleColor.Red)
            Exit Sub
        End If
        
        Dim ws As New SimpleWebServer.WebServer(Port, RootPath, AddressOf LogEvent)
        ws.StartServer()

        LogEvent("Server Running at " + RootPath + "...", ConsoleColor.Green)
        LogEvent("Type 'stop' to terminate the server.")
        While Console.ReadLine.ToLower <> "stop"
            LogEvent("Input not recognized. Type 'stop' to terminate the server.")
        End While

        ws.StopServer()
        ws = Nothing
        
    End Sub


    Private Sub LogEvent(ByVal Text As String, Optional Color As ConsoleColor = Nothing)
        If Color = Nothing Then Color = ConsoleColor.White
        Console.ForegroundColor = Color
        Console.Write(Text)
        Console.ForegroundColor = ConsoleColor.White
        Console.WriteLine()
    End Sub

End Module
